#include <string>
using namespace std;
class Car{
public:
int getRegistration() { return registration; }
string getColour() { return colour; }
void setRegistration(int pReg){
registration = pReg;
}
void setColour(string pColour){
colour = pColour;
}
private:
int registration;
string colour;
};
