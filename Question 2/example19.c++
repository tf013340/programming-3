void basicCastExample() {
	double d = 3.14;
	// static cast allows to perform implicit type conversions
	void* p = &d;
	// static cast allows to change the type of a void pointer
	int* b = static_cast<int*>(p); // Returns a value of type int
	// const cast removes const
	int const* pc = b;
	int* pw = const_cast<int*>(pc);
}
void basicReinterpretCast() {
	size_t a;
	// reinterpreting a number as a pointer, static_cast() would fail
	void* p = reinterpret_cast<void*>(4711);
	// reinterpreting the bit pattern as a pointer
	a = reinterpret_cast<size_t>(p);
}
