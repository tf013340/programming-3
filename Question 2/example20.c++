void castObject() {
	// static cast with object references
	// assume we know that a reference is of a given type.
	Child c;
	cout << "Child c: " << c.hello() << endl;
	Parent& pc = c;
	cout << "Parent & pc = c : " << pc.hello() << endl;
	// we know that pc is of type Child, this works
	Child& cr = static_cast<Child&>(pc);
	cout << "static_cast<Child&>(pc) : " << cr.hello() << endl;
	// WARNING, this works and will output Child, albeit it is only parent object
	// static cast enforces the cast even if the object isn't of the right type:
	Parent p1;
	cr = static_cast<Child&>(p1);
	cout << "static_cast<Child&>(parent); " << cr.hello() << endl;
}
