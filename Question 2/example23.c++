int main() {
	PeopleFactory factory;
	Person* p = factory.createPerson({ "works", "studies" }); // who should that be?
	Person* p2 = factory.createPerson({ "car" }); // who should that be?
	cout << typeid(*p).name() << endl;
	cout << typeid(*p2).name() << endl;
	Person* p3 = factory.createPerson({ "car", "works" }); // invalid, here maybe?
}
