void swap(int& first, int& second) {
	int temp;
	temp = first;
	first = second;
	second = temp;
}
int main() {
	int y = 5;
	int z = 3;
	swap(y, z);
	// what is a and b after the call?
	return 0;
}

Proof of Result: https://prnt.sc/26t7hcj
