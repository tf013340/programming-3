int main() {
	person boss(
		"Boss", COLOR_BLACK);
	boss.print();
	boss.dyeHair(COLOR_BLUE);
	boss.print();
	person jane;
	jane.print();
	return 0;
}
