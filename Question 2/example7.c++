bool Account::transfer(Account& to, float amount) {
	if (balance < amount) {
		return false;
	}
	to.balance += amount;
	balance -= amount;
	return true;
}
