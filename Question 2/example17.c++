int main(){
vector<string> list = { "Computers", "Scientists", "Routers", "Monitors" };
for (vector<string>::iterator i = list.begin(); i != list.end(); i++){
// could have used: "auto i = " instead
cout << i->length() << ": " << *i << endl;
// calls the string classes method length()
}
return 0;
}
